"""Example module to compute first derivatives.

This module computes the first derivatives of a given function on a
uniform grid of `N+1` nodes spanning [0,1]. From the definition of the
grid, the number of internal nodes is `N` and the spacing `Dx` is
given by `1/N`

Usage:

$ python derivs1d.py

This will create a pop-up graph with the L1 errors for the
approximation of the first derivative of a sinusoidal function.

"""

# Matplotlib for plotting
import pylab

# NumPy for array operations
import numpy
from numpy import sin, cos, pi, exp
from numpy.linalg import norm

def get_grid(N, a=0, b=1):
  """Return a uniformly spaced grid with spacing Dx

  Parameters:

  N : int
      Number of internal nodes

  a, b : int
      The default domain end points

  """
  # the uniform spacing used. Be careful to enforce floating point
  # division.
  dx = float(b - a)/N
  
  # we will use the NumPy function zeros to 'allocate' a chunk of
  # memory that will determine our trivial grid. In this example,
  # shape parameter is a tuple that tells NumPy that we want a
  # one-dimensional array of `N+1` points or nodes
  x = numpy.zeros( shape=(N+1,), dtype=float )

  # Now we populate the array with the coordinate positions
  # representing the grid. Remember the Python range function is
  # exclusive of the last index.
  for i in range(1, N+1):
    x[i] = a + i*dx

  return x  

def get_ic(x, wave=True):
  """Return the initial function defined on the grid and it's
  analytical derivative

  Parameters:
  
  x : numpy.ndarray
      The grid over which the function is defined

  For this example, we use the periodic, sinusoidal wave function:
   `0.5*sin(6*pi*x) + sin(pi*x)`

  The derivative is of course
  `3*pi*cos(6*pi*x) + pi*cos(x)`

  """
  # first, 'allocate' the array for the function. Note that since the
  # function is defined on the grid, we can safely set the shape of
  # the array to mimic that of the grid. 
  f = numpy.zeros( shape=x.size, dtype=float )

  #Alternatively, we could also use the `numpy.zeros_like`
  #function. Note that in this case, it is implicitly assumed that the
  #data type of the array is mimiced.
  fd = numpy.zeros_like(x)    

  for i in range( x.size ):
    f[i] = 0.5 * sin( 6.0*pi*x[i] ) + sin( pi * x[i] )  
  
  # Note that NumPy supports array-wide operations. So we could do:
  fd = 3*pi*cos(6*pi*x) + pi*cos(pi*x)

  return f, fd

def U_IC_step(x):
  value = zeros((M+1,))
  for i in range(0,(M+1)/2):
    value[i] = 1.0
  return value

def Initialize():
  U = zeros((M+1,))
  if ic==1:
    U   = U_IC_wave(x)
  else:
    U = U_IC_step(x)
  return U
  
def backward2(x, u):
  """Compute the second-order, backward difference, first derivative
  of the function `u` on the grid `x`:

  `du/dx = 1/2*dx * (u[i-2] - 4*u[i-1] + 3*u[i])`

  Parameters:

  x : numpy.ndarray
      The grid over which the function is defined
  
  u : numpy.ndarray
      The function for which the derivative is computed
      
  """
  # allocate an array for the derivatives
  dudx = numpy.zeros_like(x)

  # constants
  dx = x[1] - x[0]
  twodxi = 1./(2*dx)

  # compute the derivative
  dudx[2:] = twodxi * (u[:-2] - 4*u[1:-1] + 3*u[2:])
  
  # since we assume the function is periodic, we need to explicitly
  # handle the boundary points. The derivative formula uses a 3-point
  # stencil, extending two nodes to the left of the node in
  # consideration. This means that the first two nodes have values
  # that need to be adjusted, taking into account the periodicity of
  # the function.
  #dudx[0] = twodxi * (u[-2] - 4*u[-1] + 3*u[0])
  #dudx[1] = twodxi * (u[-1] - 4*u[0] + 3*u[1])
  dudx[0] = 1./dx * (u[1] - u[0])
  dudx[1] = twodxi * (u[2] - u[0])  

  return dudx

def backward3(x, u):
  """Compute the third-order, backward difference, first derivative
  of the function `u` on the grid `x`:

  `du/dx = 1/6*dx * (u[i-2] - 6*u[i-1] + 3*u[i] + 2*u[i+1])`

  Parameters:

  x : numpy.ndarray
      The grid over which the function is defined
  
  u : numpy.ndarray
      The function for which the derivative is computed
      
  """
  # allocate an array for the derivatives
  dudx = numpy.zeros_like(x)

  # constants
  dx = x[1] - x[0]
  sixdxi = 1./(6*dx)

  # compute the derivative
  dudx[2:-1] = sixdxi * (u[:-3] - 6*u[1:-2] + 3*u[2:-1] + 2*u[3:])
  
  # since we assume the function is periodic, we need to explicitly
  # handle the boundary points. The derivative formula uses a 4-point
  # stencil, extending two nodes to the left of the node in
  # consideration and one node to the right. This means that the first
  # two nodes and last nodes have values that need to be adjusted,
  # taking into account the periodicity of the function.
  #dudx[0] = sixdxi * (u[-2] - 6*u[-1] + 3*u[0] + 2*u[1])
  #dudx[1] = sixdxi * (u[-1] - 6*u[0] + 3*u[1] + 2*u[2])
  #dudx[-1] = sixdxi* (u[-3] - 6*u[-2] + 3*u[-1] + 2*u[0])

  dudx[0] = 1./dx * (u[1] - u[0])
  dudx[1] = 1./(2*dx) * (u[2] - u[0])  
  dudx[-1] = 1./dx * (u[-1] - u[-2])

  return dudx

def central2(x, u):
  """Second-order, central difference approximation to the first
  derivative
  """
  # allocate storage for the derivative
  dudx = numpy.zeros_like(x)
  
  # compute the derivative for interior poitns
  dx = x[1] - x[0]
  dudx[1:-1] = 1./(2*dx) * ( u[2:] - u[:-2] )

  # boundary conditions
  #dudx[0] = 1./(2*dx) * (u[1] - u[-1])
  #dudx[-1] = 1./(2*dx)* (u[0] - u[-2])
  dudx[0] = 1./dx * (u[1] - u[0])
  dudx[-1] = 1./dx * (u[-1] - u[-2])

  return dudx

def central4(x, u):
    """Fourth-order, central difference approximation to the first
    derivative
    """
    dudx = numpy.zeros_like(x)
    
    # derivatives on the interior nodes
    dx = x[1] - x[0]
    dudx[2:-2] = 1./(12*dx) * (u[:-4] - 8*u[1:-3] + 8*u[3:-1] - u[4:] )

    # boundary conditions
    dudx[0] = 1./dx * (u[1] - u[0])
    dudx[1] = 0.5/dx * (u[2] - u[0])
    
    dudx[-1] = 1./dx * (u[-1] - u[-2])
    dudx[-2]  = 0.5/dx * (u[-1] - u[-3])

    return dudx

def compactFD4(x, u):
  M = x.size-1
  dx = x[1] - x[0]
  dudx = numpy.zeros_like(x)

  a = numpy.zeros_like(x)
  b = numpy.zeros_like(x)
  c = numpy.zeros_like(x)
  d = numpy.zeros_like(x)
  threeoh = 3.0/dx
    
  b[0] = 1.0
  c[0] = 2.0
  d[0] = (-5./2.*u[0] + 2.*u[1] + 0.5*u[2])/dx

  for i in range(1,M):
    a[i] = 1.0
    b[i] = 4.0
    c[i] = 1.0

  for i in range(1,M):
    d[i] = threeoh*(u[i+1]-u[i-1])

  b[M] = 1.0
  a[M] = 2.0
  d[M] = (5./2.*u[M] - 2.*u[M-1] - 0.5*u[M-2])/dx
    
  TDMASolve(a, b, c, d)
    
  for i in range(0,M+1):
    dudx[i] = d[i]
 
  return dudx
       
def TDMASolve(a, b, c, d):
  n = len(d) - 1 
  c[0] /= b[0]
  d[0] /= b[0]
  for i in range(1, n):
    c[i] /= b[i] - a[i] * c[i-1]
    d[i] = (d[i] - a[i] * d[i-1]) / (b[i] - a[i] * c[i-1])
    d[n] = (d[n] - a[n] * d[n-1]) / (b[n] - a[n] * c[n-1])
  for i in reversed(range(n)):
    d[i] = d[i] - c[i] * d[i+1]
 
  return [d[i] for i in range(n+1)] # return the solution


if __name__ == '__main__':
  grid_sizes = [64, 128, 256, 512, 1024]

  backward2_errors = []
  backward3_errors = []
  central2_errors = []
  central4_errors = []
  compact_errors = []
  
  for N in grid_sizes:
    # get the grid
    x = get_grid(N)
    
    # get the wave solution and exact derivative on the grid
    func, exact_derivative = get_ic(x)
    
    # compute the numerical derivatives and errors for each scheme
    fd = backward2(x, func)
    backward2_errors.append( norm(fd-exact_derivative, 1) )

    fd = backward3(x, func)
    backward3_errors.append( norm(fd-exact_derivative, 1) )

    fd = central2(x, func)
    central2_errors.append( norm(fd-exact_derivative, 1) )

    fd = central4(x, func)
    central4_errors.append( norm(fd-exact_derivative, 1) )

    fd = compactFD4(x, func)
    compact_errors.append( norm(fd-exact_derivative, 1) )

  # now plot the errors on a log-log plot
  f = pylab.figure()
  pylab.plot(grid_sizes, backward2_errors, '.--')
  pylab.plot(grid_sizes, backward3_errors, '.--')
  pylab.plot(grid_sizes, central2_errors, '.--')
  pylab.plot(grid_sizes, central4_errors, '.--')
  pylab.plot(grid_sizes, compact_errors, '.--')

  pylab.legend(['Backward2', 'Backward3', 'Central2', 'Central4', 'CompactFD4'], 'lower left')
  ax = pylab.gca()
  ax.set_yscale('log', basey=10)
  ax.set_xscale('log', basex=2)
  
  pylab.ylabel(r'$L_1$ Error')
  pylab.xlabel(r'$N_p$')
  pylab.title(r'$L_1$ errors for first-derivative approximations')
  
  pylab.show()
